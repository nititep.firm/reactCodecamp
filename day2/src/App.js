import React, { Component } from "react";
import 'antd/dist/antd.css';
import { Button, Card, Input, Icon } from 'antd';
import "./App.css";


class App extends Component {   

    constructor(props) {
        super(props);
        this.state = {
          count: 0,
          text: '',
          chatLeft: [],
          chatRight: [],
          time: '' 

        };
      }

      onChange = (e) => {
          this.setState({text: e.target.value})
      }

      onSubmit = (e) => {
        e.preventDefault()  

        let date = new Date()
        let hour = date.getHours()
        let min = date.getMinutes()      
        
        if(this.state.count % 2 === 0){           
            this.setState({
                text: '',
                chatLeft: [...this.state.chatLeft, this.state.text],
                count: this.state.count + 1,
                time: `${hour}:${min}`
              });
        }else{            
            this.setState({
                text: '',
                chatRight: [...this.state.chatRight, this.state.text],
                count: this.state.count + 1,
                time: `${hour}:${min}`
              });    
           

            
        }
      }
    
    
    render() {
        return (
            <div>  
                <Card title="Messenger" extra={<a href="#">More</a>} style={{ width: 600 }} className= "chatCard">               
                <div className="left">
                {
                this.state.chatLeft.map((v) => <h3 key={v}><Icon type="smile-o"/> : {v} <br/> <p>{this.state.time}</p></h3>)
                } 
                </div>
                <div className="right">
                {
                this.state.chatRight.map((v) => <h3 key={v}><Icon type="smile" />: {v} <br/><p> {this.state.time}</p></h3>)
                } 
                </div>    
                </Card>     
            <form className="App" onSubmit={this.onSubmit}>   
            <div class="InputText">
            
            <Input value={this.state.text} onChange={this.onChange} />         
            <Button type="primary" style={{margin: '10px'}} onClick={this.onSubmit}>SEND</Button>
            
            </div>          

            </form>           
            <div> {this.props.text}</div>

            </div>
        );
      }
    }

export default App;
