import React, { Component } from 'react';
import {Link} from 'react-router-dom'

import styled from 'styled-components';

const StyleCPN = styled.div`

h1 {
  text-align: center;
  font-size: 70px;
  margin-top: 50px;
}
`

class Home extends Component {
  render() {
    return (
         <StyleCPN>
          <h1>Welcome to my To-Do-List</h1>
         </StyleCPN>
    )
  }
}

export default Home;