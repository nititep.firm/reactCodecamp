import React, { Component }  from 'react';
import {Link} from 'react-router-dom'
import "antd/dist/antd.css";
import { Menu, Icon } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;



class Header extends Component {

    state = {
        current: 'logo',
      }
      handleClick = (e) => {
        console.log('click ', e);
        this.setState({
          current: e.key,
        });
      }
 
    render() {
    return(
        <header>
        <div className="flexbox-container">        

   
           
        <Menu
        onClick={this.handleClick}
        selectedKeys={[this.state.current]}
        mode="horizontal"
      >
        <Menu.Item key="logo">
        <Link to="/"><Icon type="check" />TO-DO-LIST</Link>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to="/todo1"><a>Todo List Theme 1</a></Link>
        </Menu.Item>
        <Menu.Item key="2">
        <Link to="/todo2"><a>Todo List Theme 2</a></Link>
        </Menu.Item>
        <Menu.Item key="3">
        <Link to="/todo3"><a>Todo List Theme 3</a></Link>
        </Menu.Item>
        <Menu.Item key="4">
        <Link to="/todofirebase"><a>Todo List Firebase</a></Link>
        </Menu.Item>
      </Menu>
            
        </div>
        </header>
        )
    }
}

export default Header;