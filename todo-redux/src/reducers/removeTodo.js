import { REMOVE_TODO } from '../constant.js'

export default (state = [], action) => {
    switch(action.type) {
        case REMOVE_TODO:
        const { removeTodo } = action;
        return removeTodo;
    default:
        return state
    }
}