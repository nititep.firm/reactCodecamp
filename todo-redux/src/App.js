import React, { Component } from 'react';

import './App.css';
import { connect } from 'react-redux'

import { Todo } from './components/Todo'

class App extends Component {
  constructor(props){
    super(props)
  }

  render() {

    return (
        <div>
          <h1>{this.props.state.listItem}</h1>
        <div className="App">
          <Todo />        
        </div>
        <div>{this.props.propsTest}</div>
        </div>
    );
  }
}

function mapStateToProps(state){
  console.log('state', state);
  return { state }
}

export default connect(mapStateToProps, null)(App)
