import React, { Component } from 'react';
import './footer.css';

class Footer extends Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div className="footer">
                <ul>
                    <li>
                        เกี่ยวกับ
                    </li>
                    <li>
                        ศูนย์ช่วยเหลือ
                    </li>
                    <li>
                        บล็อก
                    </li>
                    <li>
                        สถานะ
                    </li>
                    <li>
                        ตำแหน่งงาน
                    </li>
                    <li>
                        ข้อตกลง
                    </li>
                    <li>
                        นโยบายเกี่ยวกับความเป็นส่วนตัว
                    </li>
                    <li>
                        คุกกี้
                    </li>
                    <li>
                        ข้อมูลโฆษณา
                    </li>
                    <li>
                        ยี่ห้อ
                    </li>
                    <li>
                        แอพ
                    </li>
                    <li>
                        ลงโฆษณา
                    </li>
                    <li>
                        การตลาด
                    </li>
                    <li>
                        ธุรกิจต่างๆ
                    </li>
                    <li>
                        นักพัฒนา
                    </li>
                    <li>
                        สารบบ
                    </li>
                    <li>
                        การตั้งค่า
                    </li>
                    <li>
                        © 2018 ทวิตเตอร์
                    </li>
                </ul>
            </div>
        );
    }
}

export default Footer;