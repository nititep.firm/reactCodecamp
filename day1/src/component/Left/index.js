import React, { Component } from 'react';
import './Left.css';
import { Icon } from 'antd'

class Left extends Component {

  constructor(props) {
    super(props)
  }

  render() {

    return (
      <div className="Left">
        <div>
          <div>
            <span><Icon type="search" /></span>
            ติดตามสิ่งที่คุณสนใจ
        </div>
          <div>
            <span><Icon type="team" /></span>
            ฟังสิ่งที่ผู้คนกำลังพูดถึง
        </div>
          <div>
            <span><Icon type="message" /></span>
            เข้าร่วมบทสนทนา
        </div>
        </div>
      </div>
    );
  }
}

export default Left;