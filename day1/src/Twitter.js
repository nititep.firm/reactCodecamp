import React, { Component } from 'react';
import './Twitter.css';

import Left from './component/Left'
import Right from './component/Right'
import Footer from './component/footer'

class Twitter extends Component {
    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div className="Twitter">
                <Left />
                <Right />
                <Footer />
            </div>
        );
    }
}

export default Twitter;